
		
    <footer class="footer">
		<div class="container">
			<div class="footer-wrap">
				<div class="footer-wrap__img">
					<img src="<?php echo get_field('footer_logo','option')['url']?>" alt="">
					<div>
						<ul class="footer-social">
							<li><a href="<?php the_field('facebook_link', 'option')?>"> <i class="fab fa-facebook-f"></i> </a></li>
							<li><a href="<?php the_field('linkedin_link', 'option')?>"> <i class="fab fa-linkedin-in"></i> </a></li>
						</ul>
					</div>
				</div>
				<div class="footer-wrap__menu">
					<div class="footer-wrap__title-wrap">
						<h5 class="footer-wrap__title">Dúležité odkazy</h5>
					</div>
					<?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'menu-1',
                            'menu_id'        => 'footer-menu-1',
                        )
                    );
                    ?>
				</div>
				<div class="footer-wrap__services">
					<div class="footer-wrap__title-wrap">
						<h5 class="footer-wrap__title">Služby</h5>
					</div>
					 <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'menu-2',
                            'menu_id'        => 'footer-menu-2',
                        )
                    );
                    ?>

				</div>
				<div class="footer-wrap__contact">
					<div class="footer-wrap__title-wrap">
						<h5 class="footer-wrap__title">Kontakt</h5>
					</div>
					<ul>
						<li> <a href="mailto:<?php the_field('kontakt_mail', 'option'); ?> "> <?php the_field('kontakt_mail', 'option'); ?> </a> </li>
						<li> <a href="tel:<?php the_field('kontakt_mobil', 'option'); ?>"><?php the_field('kontakt_mobil', 'option'); ?> </a>  </li>
					</ul>
				</div>
				<div class="footer-wrap__address">
					<div class="footer-wrap__title-wrap">
						<h5 class="footer-wrap__title">Adresa</h5>
					</div>
					<div>
						<?php the_field('adresa','option');?>
					</div>
				</div>
			</div>

		</div>
	</footer>


			<script type="text/javascript" src="<?php echo get_template_directory_uri();?>/js/animations.js?v=<?php echo time();?>"></script>
			<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
			
			<script>
			
			</script>

		<?php wp_footer(); ?>

	</body>
</html>
