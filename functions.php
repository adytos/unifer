<?php


    register_nav_menus(
        array(
            'header-menu' => esc_html__( 'Header menu', 'unifer-theme' ),
            'menu-1' => esc_html__( 'Primary', 'unifer-theme' ),
            'menu-2' => esc_html__( 'Secondary', 'unifer-theme' ),
        )
    );

    
    add_theme_support( 'post-thumbnails' );
  


//Excerpt length
    function wp_example_excerpt_length($length)
    {
        return 30;
    }
    add_filter('excerpt_length', 'wp_example_excerpt_length');

    function new_excerpt_more($more)
    {
        return '...';
    }
    add_filter('excerpt_more', 'new_excerpt_more');

   
   /* options page */
    if (function_exists('acf_add_options_page')) {

        acf_add_options_page(array(
            'page_title' 	=> 'Nastavenie témy',
            'menu_title'	=> 'Nastavenie témy',
            'menu_slug' 	=> 'theme-settings'
        ));
    }

    //Create new post type in admin menu

add_action('init', 'create_post_type');

function create_post_type()
{
	register_post_type(
		'product',
		array(
			'labels' => array(
				'name' => __('Produkty'),
				'singular_name' => __('Produkty'),
				'add_new' => __('Pridať nový produkt'),
				'add_new_item' => __('Pridať nový produkt')
			),

			'public' => true,
			'has_archive' => true,
			'menu_icon' => 'dashicons-hammer',
		    'supports' => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt'),
			'menu_position' => 5,



		)
	);
	
}



