<?php


?><!DOCTYPE html>

<html class="no-js" <?php language_attributes(); ?>>

	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1.0" >
		<link rel="profile" href="https://gmpg.org/xfn/11">
		<?php wp_head(); ?>
		<!-- Boostrap library -->
		
		<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.js"></script>

		<link href="https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;1,300&display=swap" rel="stylesheet">
		<!-- End Bootstrap library -->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.4/css/all.css" integrity="sha384-DyZ88mC6Up2uqS4h/KRgHuoeGwBcD4Ng9SiP4dIRy0EXTlnuz47vAwmeGwVChigm" crossorigin="anonymous">
		<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css?v=<?php echo time(); ?>">
		<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.css" />
		
	</head>

	


	<body <?php body_class(); ?>>

		<?php
		wp_body_open();
		?>

<header class="header">
	<div class="container-fluid">	
		<div class="header-wrap">
			<div class="header-wrap__logo">
				<a href="<?php echo get_home_url();?>"> 	
					<?php $image = get_field('hlavne_logo', 'option'); ?>
					<img src="<?php echo $image['url'] ?>" alt="">
				</a>
			</div>
			<div class="header-wrap__menu">
				<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false">
								<span class="menu__btn-bar"></span>
                            	<span class="menu__btn-bar"></span>
                          	   <span class="menu__btn-bar"></span>
                   		       
					
				</button>
				 <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'header-menu',
                            'menu_id'        => 'header-menu',
                        )
                    );
                    ?>
			</div>
			
		</div>
	</div>
</header>


<script>

</script>
	

		