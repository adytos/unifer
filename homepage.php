<?php
/**
 * Template name: Domov
 *
 */

get_header();
?>




<?php  

    get_template_part( 'sections/section', 'hero' );
    get_template_part( 'sections/section', 'services' );
    get_template_part( 'sections/section', 'accordeon' );
    get_template_part( 'sections/section', 'blog-front' );

   
?>


<?php get_footer(); ?>

