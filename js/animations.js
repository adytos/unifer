$(document).ready(function () { 
 
     /* YT popup*/
     $('.hero-popup').magnificPopup({
        disableOn: 700,
        type: 'iframe',
        mainClass: 'mfp-fade',
        removalDelay: 160,
        preloader: false,
        fixedContentPos: false
    });




    /* AKORDEON */
   $('.acordeon-item__content').hide();
   $('.acordeon-item__title').on('click',function(){
    $(this).toggleClass('open');
    $(this).siblings().slideToggle('.acordeon-item__content');
   });




    /* BLOG SLIDER*/
   $('.blog-js').slick({
      slidesToShow: 3,
      dots: true,
      arrows: false,
      infinite:true,
      autoplay:false,
        responsive: [
          {
            breakpoint: 1199,
            settings: {
              arrows: false,
              
              slidesToShow: 2
            }
          },
          {
            breakpoint: 767,
            settings: {
              arrows: false,
             
              slidesToShow: 1
            }
          }
        ]
      });

      /* mobile menu*/
      $('.menu-toggle ').on('click',function(){
         $('body').toggleClass('opened');
      })
      
});
