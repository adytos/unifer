<section class="qaa">
    <div class="container">
        <div class="qaa-title">
            <h3 class="main-title">
               <?php the_field('nadpis_sekcie');?>
            </h3>
            <p>
                <?php the_field('podnadpis_sekcie');?>
            </p>
        </div>
        <div class="acordeon">
             <?php
                if(have_rows('akordeon') ):
                    while( have_rows('akordeon') ) : the_row();
                ?>
                 <div class="acordeon-item">
                    <div class="acordeon-item__title">
                        <h5>
                           <?php the_sub_field('nadpis'); ?>
                        </h5>
                       <i class="fas fa-caret-down"></i>
                    </div>
                    <div class="acordeon-item__content">
                        <p>
                         <?php the_sub_field('text'); ?>
                        </p>
                    </div>
                </div>                     
            <?php
                  endwhile;
                endif;
            ?>         
        </div>
    </div>
</section>