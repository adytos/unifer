<section class="blog-front">
    <div class="container">
        <div class="main-title__wrap">
            <h5 class="main-title">
                Nepřehlédněte
            </h5>
        </div>
        <div class="blog-front__items blog-js">
            <?php

            $args = [
                'post_type' => 'post',
                "posts_per_page" => 5,
                //'order' => 'ASC'
            ];
            $query = new WP_Query($args);

            while ($query->have_posts()) : $query->the_post();
            ?>
               <a href="<?php the_permalink();?>" class="blog-box ">
                   <div class="blog-box__content">
                        <div class="blog-box__content-text">
                            <h5><?php the_field('support_title');?></h5>
                            <h6><?php the_title();?><i class="fas fa-caret-right"></i></h6> 
                        </div>
                        <div class="blog-box__content-info">
                            <div class="">
                                <?php the_date();?>
                            </div>
                            <div>
                                <i class="far fa-clock"></i> <span> <?php the_field('dlzka_citania');?></span>
                            </div>
                        </div>
                   </div>
                   <div class="blog-box__img">
                        <?php  the_post_thumbnail(); ?>
                   </div>
               </a> 
                <?php wp_reset_postdata() ?>
            <?php endwhile; ?>
        </div>
        <div class="blog-front__btn">
            <a href="<?php the_permalink(76)?>">
                Vice na blogu <i class="fas fa-caret-right"></i>
            </a>
        </div>  
    </div>
</section>