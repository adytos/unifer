<section class="hero">
    <div class="container">
        <div class="hero-wrap">
            <div class="hero-wrap__left">
                <h1> <?php the_field('hlavny_nadpis')?></h1>
                <p> <?php the_field('hlavny_podnadpis');?> 
                </p>
                 <div class="blog-front__btn hero-wrap__left-btn">
                    <a href="<?php the_field('hlavny_btn_link');?>">
                         <?php the_field('text_buttonu');?> <i class="fas fa-caret-right"></i>
                    </a>
                </div>
            </div>
            <div class="hero-wrap__right">
                <a class="hero-popup" href="<?php the_field('video_popup');?>">
                    <img src="<?php echo get_field('hlavny_obrazok')['url'];?>" alt="">
                </a>
            </div>
        </div>
    </div>
</section>