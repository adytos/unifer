<section class="services">
    <div class="container">
        <div class="main-title">
            <h3>
                Produkty a služby
            </h3>
        </div>
    </div>
    <div class="services-wrap">
          <?php
            $args = [
                'post_type' => 'product',
                "posts_per_page" => 4,
                'order' => 'ASC'
            ];
            $query = new WP_Query($args);

            while ($query->have_posts()) : $query->the_post();
            ?>
                <a class="services-wrap__box" href="<?php the_permalink();?>">
                    <div >
                            <div class="services-wrap__box-img">
                                <?php  the_post_thumbnail(); ?>
                            
                            </div>
                            <div class="services-wrap__box-content">
                                <h5><?php the_title();?></h5>
                                <p><?php the_field('front_text')?></p>
                            </div>
                    </div>
                </a>    
                <?php wp_reset_postdata() ?>
            <?php endwhile; ?>
    </div>
</section>